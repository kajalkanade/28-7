package com.cdac.springwebapp.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.cdac.springwebapp.controller.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.cdac.springwebapp.dto.User;
import com.cdac.springwebapp.serv.UserService;

@Controller
public class UserController {

	@Autowired
	private UserService userService;
	// private int a;
	String previous;

	@RequestMapping(value = "/prep_login_form.htm")
	public String prepLoginForm(ModelMap model) {
		model.put("user", new User());
		model.put("vendork", new User());
		return "index";
	}

	@RequestMapping(value = "/prep_reg_form.htm")
	public String prepRegForm(ModelMap model) {
		model.put("user", new User());
		return "reg_form";
	}

	@RequestMapping(value = "/reg.htm")
	public String register(User user, ModelMap model) {
		// System.out.println("inside reg");
		user.setUserPass(Encryption.encrypt(user.getUserPass()));
		user.setUserConformPass(Encryption.encrypt(user.getUserConformPass()));
		userService.createUser(user);
		// model.put("user", new User());
		// model.put("vendork", new User());
		return "index";
	}

	@RequestMapping(value = "/loginU.htm")
	public String login(User user, ModelMap model, HttpSession session) {

		user.setUserPass(Encryption.encrypt(user.getUserPass()));
		boolean b = userService.checkUser(user);
		if (b) {
			User ulist1 = userService.selectUser(user);
			session.setAttribute("user", ulist1);
			previous = user.getUserName();
			// model.put("user",ulist1);
			// User ulist1 = userService.selectUser(user);
			return "index";
		}
		// model.put("vendork", new User());
		// model.put("user", new User());
		return "index";
	}

	@RequestMapping(value = "/loginV.htm")
	public String loginV(User user, ModelMap model, HttpSession session) {

		user.setUserPass(Encryption.encrypt(user.getUserPass()));
		boolean b = userService.checkVendor(user);
		if (b) {
			User vendor = (User) userService.getVendorserv(user);
			System.out.println(vendor.getUserId());
			session.setAttribute("vendork", vendor);
			model.put("ul", vendor);
			return "vendor_home";
		}
		// model.put("user", new User());
		if (session.getAttribute("vendork") != null) {
			return "vendor_home";
		}
		return "index";
	}

	@RequestMapping(value = "/logoutU.htm")
	public String logout(User user, HttpSession session, ModelMap model) {
		if (session.getAttribute("user") == null) {
			return "index";
		}
		User u = (User) session.getAttribute("user");
		// System.out.println(u.getActive());

		// user.setUserPass(Encryption.encrypt(user.getUserPass()));
		// User ulist1 = userService.selectUser(object);

		u.setActive(0);
		userService.logoutSetStatus(u);
		session.removeAttribute("user");
		session.invalidate();

		// model.put("vendork", new User());
		// model.put("user", new User());
		return "index";
	}

	@RequestMapping(value = "/logoutV.htm")
	public String logoutV(User user, HttpSession session, ModelMap model) {
		if (session.getAttribute("vendork") == null) {
			return "index";
		}
		User v = (User) session.getAttribute("vendork");
		// System.out.println(u.getActive());

		// user.setUserPass(Encryption.encrypt(user.getUserPass()));
		// User ulist1 = userService.selectUser(object);

		v.setActive(0);
		userService.logoutSetStatus(v);
		session.removeAttribute("vendork");
		// model.put("ul", v);
		session.invalidate();

//	model.put("vendork", new User());
		// model.put("user", new User());
		return "index";
	}

	@RequestMapping(value = "/update.htm")
	public String update(User user, HttpSession session) {
		// User user1= (User) session.getAttribute("vendork");
		// model.put("ul", user1);
		// user.getAddress();
		// System.out.println(user.getUserProfession());
		userService.modifyUser(user);
		return "vendor_home";
	}

	@RequestMapping(value = "/user_list.htm")
	public String userList(User user, ModelMap model) {
		List<User> ulist = userService.selectAllUsers();
		model.put("ul", ulist);
		return "user_list";
	}

	@RequestMapping(value = "/vendor_list.htm")
	public String vendorList(User user, ModelMap model, HttpSession session, @RequestParam("arr") String arr) {
		if (session.getAttribute("user") == null) {
			return "index";
		}
		System.out.println(arr);
		List<User> ulist1 = userService.selectAllVendor(user);
		model.put("ul1", ulist1);
		model.put("subServList", arr);
		return "vendor_list";
	}

	@RequestMapping(value = "/delete_user.htm")
	public String deleteUser(@RequestParam int userId, ModelMap model) {
		userService.removeUser(userId);
		List<User> ulist = userService.selectAllUsers();
		model.put("ul", ulist);
		return "user_list";
	}

}