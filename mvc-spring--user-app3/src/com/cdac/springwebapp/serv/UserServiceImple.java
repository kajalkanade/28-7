package com.cdac.springwebapp.serv;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cdac.springwebapp.dao.UserDao;
import com.cdac.springwebapp.dto.User;




@Service
public class UserServiceImple implements UserService {
	@Autowired
	private UserDao userDao;
	
	
	public void createUser(User user) {
		userDao.save(user);
	}
	
	public void removeUser(int userId) {
		userDao.deleteById(userId);
	}
	
	public void modifyUser(User user) {
		userDao.update(user);
	}
	
	public List<User> selectAllUsers() {
		return userDao.getAll();
	}
	
	public List<User> selectAllVendor(User user) {
		return userDao.getAllVendor(user);
	}
	
	@Override
	public boolean checkUser(User user) {
		return userDao.validateUser(user);
	}
	
	public boolean checkVendor(User user) {
		return userDao.validateVendor(user);
	}

	@Override
	public User getVendorserv(User user) {
		return userDao.getVendorimple(user);
		
	}

	@Override
	public User selectUser(User user) {
		return userDao.getUser(user);
		
	}

	@Override
	public void logoutSetStatus(User user) {
		userDao.logoutStatus(user);
		
	}
}