package com.cdac.springwebapp.serv;

import java.util.List;

import com.cdac.springwebapp.dto.User;



public interface UserService {	
	public void createUser(User user);
	public void logoutSetStatus(User user);
	public void removeUser(int userId);
	public void modifyUser(User user);
	public List<User> selectAllUsers();
	public boolean checkUser(User user);
	public boolean checkVendor(User user);
	public List<User> selectAllVendor(User user);
	public User getVendorserv(User user);
	public User selectUser(User user);
}