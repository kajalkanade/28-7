package com.cdac.springwebapp.dao;

import java.util.List;

import com.cdac.springwebapp.dto.User;


public interface UserDao {	
	public void save(User user);
	public void logoutStatus(User user);
	public void deleteById(int userId);	
	public void update(User user);
	public List<User> getAll();
	public boolean validateUser(User user);
	public boolean validateVendor(User user);
	public List<User> getAllVendor(User user);
	public User getVendorimple(User user);
	public User getUser(User user);
}
