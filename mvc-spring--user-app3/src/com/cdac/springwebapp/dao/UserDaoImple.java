package com.cdac.springwebapp.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.cdac.springwebapp.dto.User;






@Repository
public class UserDaoImple implements UserDao {

	@Autowired
	private HibernateTemplate hibernateTemplate;
	
	public void save(final User user) {
		hibernateTemplate.execute(new HibernateCallback<List<User>>() {
			public List<User> doInHibernate(Session session) throws HibernateException {
				Transaction t = session.beginTransaction();
				session.save(user);
				t.commit();
				session.flush();
				session.close();
				return null;
			}
		});
	}
	
	public void logoutStatus(final User user) {

		
		user.setActive(0);
		hibernateTemplate.execute(new HibernateCallback<List<User>>() {
			public List<User> doInHibernate(Session session) throws HibernateException {
				Transaction t = session.beginTransaction();
				session.update(user);
				t.commit();
				session.flush();
				session.close();
				return null;
			}
		});
	}

	public void deleteById(final int userId) {
		hibernateTemplate.execute(new HibernateCallback<List<User>>() {
			public List<User> doInHibernate(Session session) throws HibernateException {
				Transaction t = session.beginTransaction();
				session.delete(new User(userId));
				t.commit();
				session.flush();
				session.close();
				return null;
			}
		});
	}

	public void update(final User user) {
		
		hibernateTemplate.execute(new HibernateCallback<List<User>>() {
			public List<User> doInHibernate(Session session) throws HibernateException {
				Transaction t = session.beginTransaction();
				System.out.println(user.getUserId());
				session.update(user);
				t.commit();
				session.flush();
				session.close();
				return null;
			}
		});
	}

	public List<User> getAll() {
		List<User> ul = hibernateTemplate.execute(new HibernateCallback<List<User>>() {
			public List<User> doInHibernate(Session session) throws HibernateException {
				Transaction t = session.beginTransaction();
				Query q = session.createQuery("from User");
				List<User> userList = q.list();
				t.commit();
				session.flush();
				session.close();
				return userList;
			}
		});
		return ul;
	}
	
	public List<User> getAllVendor(User user) {
		List<User> ul = hibernateTemplate.execute(new HibernateCallback<List<User>>() {
			public List<User> doInHibernate(Session session) throws HibernateException {
				Transaction t = session.beginTransaction();
				Query q = session.createQuery("from User where userRole = ? and active = ? ");
				q.setString(0,"Vendor");
				q.setInteger(1, 1);
				List<User> userList = q.list();
				t.commit();
				session.flush();
				session.close();
				return userList;
			}
		});
		return ul;
	}
	
	@Override
	public boolean validateUser(User user) {
		List<User> ul = hibernateTemplate.execute(new HibernateCallback<List<User>>() {
			public List<User> doInHibernate(Session session) throws HibernateException {
				Transaction t = session.beginTransaction();
				Query q = session.createQuery("from User where userName = ? and userPass = ? and phoneNo = ?  and userRole = ? " );
				q.setString(0, user.getUserName());
				q.setString(1, user.getUserPass());
				q.setString(2, user.getPhoneNo());
				q.setString(3, "User");
				List<User> userList = q.list();// no need of list
				t.commit();
				session.flush();
				session.close();
				return userList;
			}
		});
		if(!ul.isEmpty()) {
			
			User u= ul.get(0);
			u.setActive(1);
			hibernateTemplate.execute(new HibernateCallback<List<User>>() {
				public List<User> doInHibernate(Session session) throws HibernateException {
					Transaction t = session.beginTransaction();
					session.update(u);
					t.commit();
					session.flush();
					session.close();
					return null;
				}
			});
			return true;
		}	
		return false;
	}
	
	public boolean validateVendor(User user) {
		List<User> ul = hibernateTemplate.execute(new HibernateCallback<List<User>>() {
			public List<User> doInHibernate(Session session) throws HibernateException {
				Transaction t = session.beginTransaction();
				Query q = session.createQuery("from User where userName = ? and userPass = ? and phoneNo = ?  and userRole = ?");
				q.setString(0, user.getUserName());
				q.setString(1, user.getUserPass());
				q.setString(2, user.getPhoneNo());
				q.setString(3, "Vendor");
				List<User> userList = q.list();
				t.commit();
				session.flush();
				session.close();
				return userList;
			}
		});
		if(!ul.isEmpty()) {
			
			User u= ul.get(0);
			u.setActive(1);
			hibernateTemplate.execute(new HibernateCallback<List<User>>() {
				public List<User> doInHibernate(Session session) throws HibernateException {
					Transaction t = session.beginTransaction();
					session.update(u);
					t.commit();
					session.flush();
					session.close();
					return null;
				}
			});
			return true;
		}	
		return false;
	}

	@Override
	public User getVendorimple(User user) {
		List<User> ul = hibernateTemplate.execute(new HibernateCallback<List<User>>() {
			public List<User> doInHibernate(Session session) throws HibernateException {
				Transaction t = session.beginTransaction();
				Query q = session.createQuery("from User where userName = ? and userPass = ?");
				q.setString(0, user.getUserName());
				q.setString(1, user.getUserPass());
				List<User> userList = q.list();
				t.commit();
				session.flush();
				session.close();
				return userList;
			}
		});
		
		return ul.get(0);
	}

	@Override
	public User getUser(User user) {
		List<User> ul = hibernateTemplate.execute(new HibernateCallback<List<User>>() {
			public List<User> doInHibernate(Session session) throws HibernateException {
				Transaction t = session.beginTransaction();
				Query q = session.createQuery("from User where userName = ? and userPass = ?");
				q.setString(0, user.getUserName());
				q.setString(1, user.getUserPass());
				List<User> userList = q.list();
				t.commit();
				session.flush();
				session.close();
				return userList;
			}
		});
		return ul.get(0);
	}
	
	
}