package com.cdac.springwebapp.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Columns;

@Entity
@Table(name = "user")
public class User {
	@Id
	@Column(name = "user_id")
	@GeneratedValue
	private int userId;
	@Column(name = "user_name")
	private String userName;
	@Column(name= "age")
	private int age;
	@Column(name= "phone_no")
	private String phoneNo;
	@Column(name="email_id")
	private String emailId;
	@Column(name = "user_pass")
	private String userPass;
	@Column(name = "user_conform_pass")
	private String userConformPass;
	@Column(name = "user_role")
	private String userRole;
	@Column(name = "user_profession")
	private String userProfession;
	@Column(name = "about")
	private String about;
	@Column(name = "address")
	private String address;
	@Column(name = "active")
	private int active;
	
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	


	public User(int userId) {
		super();
		this.userId = userId;
	}









	public User(int userId, String userName, int age, String phoneNo, String emailId, String userPass,
			String userConformPass, String userRole, String userProfession, String about, String address, int active) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.age = age;
		this.phoneNo = phoneNo;
		this.emailId = emailId;
		this.userPass = userPass;
		this.userConformPass = userConformPass;
		this.userRole = userRole;
		this.userProfession = userProfession;
		this.about = about;
		this.address = address;
		this.active = active;
	}

	public String getUserProfession() {
		return userProfession;
	}

	public void setUserProfession(String userProfession) {
		this.userProfession = userProfession;
	}




	public int getUserId() {
		return userId;
	}




	public void setUserId(int userId) {
		this.userId = userId;
	}




	public String getUserName() {
		return userName;
	}




	public void setUserName(String userName) {
		this.userName = userName;
	}




	public int getAge() {
		return age;
	}




	public void setAge(int age) {
		this.age = age;
	}




	public String getPhoneNo() {
		return phoneNo;
	}




	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}




	public String getEmailId() {
		return emailId;
	}




	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}




	public String getUserPass() {
		return userPass;
	}




	public void setUserPass(String userPass) {
		this.userPass = userPass;
	}




	public String getUserConformPass() {
		return userConformPass;
	}




	public void setUserConformPass(String userConformPass) {
		this.userConformPass = userConformPass;
	}




	public String getUserRole() {
		return userRole;
	}




	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}




	public String getAbout() {
		return about;
	}




	public void setAbout(String about) {
		this.about = about;
	}




	public String getAddress() {
		return address;
	}




	public void setAddress(String address) {
		this.address = address;
	}




	public int getActive() {
		return active;
	}




	public void setActive(int active) {
		this.active = active;
	}




	@Override
	public String toString() {
		return "User [userId=" + userId + ", userName=" + userName + ", age=" + age + ", phoneNo=" + phoneNo
				+ ", emailId=" + emailId + ", userPass=" + userPass + ", userConformPass=" + userConformPass
				+ ", userRole=" + userRole + ", about=" + about + ", address=" + address + ", active=" + active + "]";
	}




	



	
	
	
	
}