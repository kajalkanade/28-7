package com.cdac.springwebapp.dto;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Service {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int serviceId;
	private String serviceName;
	
	
	@OneToMany
	private Collection<SubServices> st = new ArrayList<SubServices>();
	
	public Collection<SubServices> getSt() {
		return st;
	}

	public void setSt(Collection<SubServices> st) {
		this.st = st;
	}

	public Service() {
		
		}
 
	public Service(int serviceId, String serviceName) {
		super();
		this.serviceId = serviceId;
		this.serviceName = serviceName;
	}

	public int getServiceId() {
		return serviceId;
	}
	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
}
