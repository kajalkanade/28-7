package com.cdac.springwebapp.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class SubServices{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int subServiceId;
	private String subServiceName;
	private float price;
	
	public SubServices() {
		// TODO Auto-generated constructor stub
	}

	public int getSubServiceId() {
		return subServiceId;
	}
	public void setSubServiceId(int subServiceId) {
		this.subServiceId = subServiceId;
	}
	public String getSubServiceName() {
		return subServiceName;
	}
	public void setSubServiceName(String subServiceName) {
		this.subServiceName = subServiceName;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
}
