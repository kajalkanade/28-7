package com.cdac.springwebapp.dto;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import com.cdac.springwebapp.dto.*;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Order {

	@Id
	private int orderId;
	@JoinColumn(name = "user_id")
	@OneToOne(cascade = CascadeType.ALL)
	private User user;
	@JoinColumn(name = "user_id")
	@OneToOne(cascade = CascadeType.ALL)
	private User vendor;
	@OneToMany
	//private Collection<SubServices> subServ = new ArrayList<SubServices>();
	private boolean orderStatus;
	private Date date;
	private Time time;
	private boolean accept_reject;
	
}
