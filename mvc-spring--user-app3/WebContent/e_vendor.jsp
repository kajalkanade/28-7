<%@page import="com.cdac.springwebapp.dto.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    	<%User vendor = (User)session.getAttribute("vendork");
   if(session.getAttribute("vendork")==null){ response.sendRedirect("index.jsp");}
    	%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
     crossorigin="anonymous">
     
   
</head>
<body>
    <div class="container-fluid ">
    <div class="row">
        <div class="col-3"></div>
        <div class="col-6 rounded bg-info mt-5">
            <h2 class=" d-flex justify-content-center"><%=vendor.getUserName() %></h2>
            <p class="d-flex justify-content-center"><strong><%=vendor.getAbout() %></strong> Web Designer / UI. </p>
            <p class=" d-flex justify-content-center"><strong>Address:  </strong><%=vendor.getAddress() %></p>
            <p class="d-flex justify-content-center"><strong>Phone No.: </strong><%=vendor.getPhoneNo() %></p>
            <div class="row">
                <div class="col-3 d-flex justify-content-center mb-2">
                        <button class="bg-primary" onclick="requestsList()"> Update</button>
                </div>
                <div class="col-3 d-flex justify-content-center mb-2">
                        <button class="bg-primary" onclick="requestsList()"> Requests</button>
                </div>
                <div class="col-3 d-flex justify-content-center mb-2">
                        <button class="bg-primary" onclick="reviews()"> Reviews</button>
                </div>
                <div class="col-3 d-flex justify-content-center mb-2">
                        <button class="bg-primary" onclick="requestsList()"> Tips</button>
                </div>
            </div>
          
            
        </div>
        <div class="col-3"  ></div>
            
        </div>
        <div class="row mt-2">
                <div class="col-3"></div>
                <div class="col-6 rounded bg-info mt-1">
                        <div id="div2" class="mt-3" style="display: none">
                                <table  class="table">
                                        <thead class="thead-dark">
                                                <tr>
                                                        <th>User Name</th>
                                                        <th>Description</th>
                                                        <th>Address</th>
                                                        <th>Accept</th>
                                                        <th>Reject</th>
                                                </tr>
                                        </thead>
                                        <tbody>
                                              <%
                                              			for(int i=0; i<5;i++){%>
                                              				<tr><td><%=i%> </td></tr>
                                              			<%}%>
                                        </tbody>
                                </table>
                        </div>
                </div>
                <div class="col-3" ></div>
        </div>
       
    </div>


   
    <script>
    function requestsList(){
    	 document.getElementById("div3");
    }
            
    function reviews() {
    document.getElementById("div2").style.display ="block"; 
    }
    
   </script>
</body>
</html>