<%@page import="com.cdac.springwebapp.dto.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  
 
 <%@ taglib uri="http://www.springframework.org/tags/form" prefix="spr" %>   
    
     <%
     
 	User user  = (User)session.getAttribute("user");
    
    	  response.setHeader("Cache-Control","no-cache, no-store,must-revalidate");
    	 response.setHeader("Pragma","no-cache");
    	 response.setHeader("Expires", "0"); 
    	
 %>   
    
<!DOCTYPE html>
<html>
<head>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<% if(user!=null) {%>
<script>
    if ( window.history.replaceState ) {
        window.history.replaceState( null,null, window.location.href );
        	
    }
</script>
<%} %>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
 <style>
    /*.dropdown-submenu {
          position: relative;
        }
        
        .dropdown-submenu .dropdown-menu {
          top: 0;
          left: 100%;
          margin-top: -1px;
        } */

        #overlay ,#overlay1,#overlay2,#overlay3,#overlay4{
  position: fixed;
  display: none;
  width: 100%;
  height: 100%;
  position: fixed; 
  overflow-y: scroll;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0,0,0,0.5);
  z-index: 2;
  cursor: pointer;
}

    body {
      font-family: 'Roboto Mono', monospace;
      background-color: #adadad;
    }

    .navbar {
      height: 46px;
      background-color: #1b1b1b;
    }

    .navbar a {
      float: left;
      font-size: 16px;
      color: white;
      text-align: center;
      padding: 14px 16px;
      text-decoration: none;
    }



    /*.container {
  max-width: 800px;
  height: 800px;
  background-color: white;
  margin: 10px auto 0 auto;
}*/
  </style>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<div class="container-fluid">

    <!-- TESTING-->


    <!-- HEADER -->
    <div class="row bg-dark" style="height:70px">
      <div class="col-4 d-flex justify-content-center align-items-center" style="font-size: 2rem;font-size: bold; font-family: cursive;color:white">
        EASE HOME
      </div>
      <div class="col-8">

        <div class="row" style="height:60px">
          <div class="col-2 d-flex justify-content align-items-center">
              <!--<select class="btn bg-light">
                  <option class="form-control" value="user">User</option>
                  <option class="form-control" value="vendor">Vendor</option>
              </select>-->
          </div>
          <div class="col-2 d-flex justify-content align-items-center">
             <!--<% if(user!=null){ %><a href="e_homepg.html" style="color:white">My Bookings</a>
             	 <% } %>-->
          </div>
          <div class="col-2 d-flex justify-content align-items-center">
           <!--<% if(user!=null){ %>  <a  href="e_homepg.html" style="color:white">My Bookings</a>
           	 <% } %>-->
          </div>
          <div class="col-2 d-flex justify-content align-items-center">
            <!--<a class="text-muted" href="e_homepg.html">My Bookings</a>-->
           <% if (user!=null){%>
           		<h1 style="color: white;"><%=user.getUserName() %></h1>
           	<% }%>
           </div>
           <div class="col-2 d-flex justify-content align-items-center">
              <% if (user==null){%><button class="btn btn-secondary" onclick="on1()">SignUp</button>
              <% }%>
            </div>
          <div class="col-2 d-flex justify-content align-items-center">
          <% if(user!=null){ %>
   <a class="btn btn-secondary" id="hii" href="logoutU.htm" > logout</a>
             	<% } else{%>
             	 <button class="btn btn-secondary"  onclick="on()"> Login </button>
             	 <% } %>
             	 
          </div>
        </div>

      </div>
    </div>

    <!-- CAROUSEL -->
    <div class="row">
      <div class="col-12" style="background-image: url('11.jpg');
          background-position:center;
          background-repeat:no-repeat;
          background-size:cover;
          height:350px;
          width:200px;"
          >


        <div class="row">
            <div class="col-12" style="margin-top: 45px;display: flex; align-items: center; justify-content: center">
                    <h1>Your Service Expert</h1>
            </div>
        </div>
  

        <!--<select class="ml-5 mt-5" >
                
           <option value="Electrician"></a>Electrician</a></option>
           <option value="Plumber"><a href="https://www.w3schools.com">Plumber</a></option>
           <option value="Cleaner"><a href="https://www.w3schools.com">Cleaner</a></option>
         

               
              </select> -->


              <div class="row">
                <div class="col-12 for" style="display: flex; align-items: center; justify-content: center">
                <select class="btn btn-secondary mt-3" id="mySelect" onchange="myFunction()" style="width: 17rem; height: 3rem;">
                  <option value= "nothing"> --select services--</option>
                  <option    value="electrician" >Electrician</option>
                  <option value="plumber">Plumber</option>
                  <option value="cleaner">Cleaner</option>
                </select>
                </div>
              </div>


      </div>

    </div>


    <!--  ICONS -->
    <div class="row">
        <div class="col-12">
            <section></section>
        </div>
    </div>
  </div>

  <!--Trying.. login.-->
  <div class=" " id="overlay" style="display: none;" >
      <div class="container-fluid">
          <div class="row" >
              <div class="col-3"  onclick="off()"></div>
              <div class="col-6" style="margin: 6px 6px 6px 6px ;" >
              <spr:form  action="loginU.htm"  commandName="user" class="bg-light" style="border: black solid">
                  <div style="margin-left: 230px">
                     
           	  <h1 style="color: black;">Login</h1>
           		
                  </div>
                  
                  <div class="form-group row ml-1 ">
                      <label for="inputName" class="col-sm-2 col-form-label">Username</label>
                      <div class="col-sm-10">
                      <spr:input type="text" path="userName" class="form-control"  placeholder="Full Name"/>
                      </div>
                  </div>
                  <div class="form-group row ml-1 ">
                      <label for="inputPassword" class="col-sm-2 col-form-label">Phone no</label>
                      <div class="col-sm-10">
                      <spr:input type="text" path="phoneNo" class="form-control" placeholder="Enter your 10 digit number"/>
                      </div>
                  </div>
                  <div class="form-group row ml-1 ">
                          <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
                          <div class="col-sm-10">
                          <spr:input type="password" path="userPass" class="form-control" placeholder="enter your password"/>
                          </div>
                      </div>
                    
  
                              <div>
                                  <input type="submit"  class="btn btn-secondary" style="margin-left: 130px;margin-bottom: 10px;width: 150px" value="login as Customer" />
                                  <input type="submit"  formaction="loginV.htm" class="btn btn-secondary" style="margin-left: 130px;margin-bottom: 10px;width: 150px" value="login as Vendor"/>
                              </div>
              </spr:form>
              </div>
              <div class="col-3"  onclick="off()"></div>
          </div>
      </div>
  
  </div>

  <!-- Trying signup form-->
  <div class="" id="overlay1"  style="display: none;" >
      <div class="container-fluid" >
          <div class="row">
          <div class="col-3" onclick="off1()"></div>
          <div class="col-6 mt-3" style="border: black 5px solid;background: rgb(186, 190, 192)" >
          <spr:form  action="reg.htm" commandName="user">
          <div>
          <h1 style="margin-left: 230px ">SignUp Form</h1>
          </div>
          <div class="form-group row">
          <label for="inputName" class="col-sm-2 col-form-label">name</label>
          <div class="col-sm-10">
          <input id="inputName" onblur="firstName()" path="userName" name="userName" placeholder="Enter your full Name" type="text" class="form-control" value=""/><span id="mSpan1"> </span>
          </div>
          </div>
          <div class="form-group row">
          <label for="inputAge" class="col-sm-2 col-form-label">age</label>
          <div class="col-sm-10">
          <input id="inputAge"  onblur="validateAge()" path="age" name="age" placeholder="Enter your age" type="text" class="form-control" /><span id="mSpan2"> </span>
          </div>
          </div>
          <div class="form-group row">
          <label for="inputPhone" class="col-sm-2 col-form-label">Phone no</label>
          <div class="col-sm-10">
          <input id="inputPhone" onblur="val1()" path="phoneNo" name="phoneNo" placeholder="Enter your 10 digit number" type="text" class="form-control" value=""/><span id="mSpan"> </span>
          </div>
          </div>
          <div class="form-group row">
          <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
          <div class="col-sm-10">
          <input id="staticEmail" onblur="emailVal()" path="emailId" name="emailId" placeholder="Enter your email id" type="text" class="form-control" value=""/><span id="mSpan3"> </span>
          </div>
          </div>
          <div class="form-group row">
          <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
          <div class="col-sm-10">
          <input id="inputPassword" onblur="passwordFun()" path="userPass" name="userPass" placeholder="enter your password" type="text" class="form-control" value=""/><span id="mSpan4"> </span>
          </div>
          </div>
          <div class="form-group row">
          <label for="inputPassword" class="col-sm-2 col-form-label">Confirm Password</label>
          <div class="col-sm-10">
          <input id="inputPassword1" onblur="conValidate()" path="userConformPass" name="userConformPass" placeholder="enter your password" type="password" class="form-control" value=""/><span id="mSpan5"> </span>
          </div>
          </div>
          <div class="form-group row">
          <label for="inputProf" class="col-sm-2 col-form-label">Profession</label>
          <div class="col-sm-10">
          <spr:select id="userRole" path="userRole" name="userRole" style="width: 150px">
          <spr:option value="Vendor">Vendor</spr:option>
          <spr:option value="User">User</spr:option>
          </spr:select>
          </div>
          </div>
          <div class="form-group row">
          <label for="About" class="col-sm-2 col-form-label">About</label>
          <div class="col-sm-10">
          <spr:textarea  path="about"  class="form-control" rows="2" placeholder="enter about"></spr:textarea>
          </div>
          </div>
          <div class="form-group row">
          <label for="Address" class="col-sm-2 col-form-label">Address</label>
          <div class="col-sm-10">
         <input onblur="addressFun()" id="add" path="address" class="form-control" rows="2" placeholder="enter address"><span id="mSpan6"> </span>
          </div>
          </div>
          <input type="submit" value="submit" class="btn btn-success" style="margin-left: 170px"/>
          </spr:form>
          </div>
          <div class="col-3"onclick="off1()"></div>
          </div>
          </div>
          
  </div>

  <!--Trying 3 Electrician-->
  <div class="" id="overlay2"  style="display: none;" >
      <div class="row">
        <div class="col-3" onclick="off2()"  >
           
            
        </div>
        <div class="col-6">
          <div class=" row bg-secondary fixed-top sticky-top" style="height: 70px">
            
           
            
          
          <div class=" col-12  d-flex justify-content-center align-items-center "> 
             <h1> Electrician</h1>
           
       
        </div>
        </div>
        <div class="row ">
            <!-- <div class="col-3 " >
                <div class="btn btn-dark " style="position:sticky;margin-left:100px; top:500px; cursor: pointer">
                  BACK
              </div>
            </div> -->
        <div class="col-12">
          <div class="card mt-2" style="width: 40rem;">
         
            <div class="card-body">
              <h5 class="card-title">Fan Repair</h5>
              
              <div class=" d-flex  " >
              <p class="card-text ">Some </p>
            </div>
            <div class="d-flex  justify-content-end">
              <input type="checkbox" name="electrician" value="fanRepair" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
            </div>
          
            </div>
          </div>
          <div class="card mt-2" style="width: 40rem;">
         
            <div class="card-body">
              <h5 class="card-title">Tubelight</h5>
              
              <div class=" d-flex  " >
              <p class="card-text ">Some </p>
            </div>
            <div class="d-flex  justify-content-end">
              <input type="checkbox" name="electrician" value="tubelight" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
            </div>
         
          
            </div>
          </div>
          <div class="card mt-2" style="width: 40rem;">
         
            <div class="card-body">
              <h5 class="card-title">Geyser</h5>
              
              <div class=" d-flex  " >
              <p class="card-text ">Some </p>
            </div>
            <div class="d-flex  justify-content-end">
              <input type="checkbox" name="electrician" value="geyser" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
            </div>
         
          
            </div>
          </div>
          <div class="card mt-2" style="width: 40rem;">
         
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              
              <div class=" d-flex  " >
              <p class="card-text ">Some </p>
            </div>
            <div class="d-flex  justify-content-end">
              <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
            </div>
         
          
            </div>
          </div>
          <div class="card mt-2" style="width: 40rem;">
         
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              
              <div class=" d-flex  " >
              <p class="card-text ">Some </p>
            </div>
            <div class="d-flex  justify-content-end">
              <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
            </div>
         
          
            </div>
          </div>
          <div class="card mt-2" style="width: 40rem;">
         
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              
              <div class=" d-flex  " >
              <p class="card-text ">Some </p>
            </div>
            <div class="d-flex  justify-content-end">
              <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
            </div>
         
          
            </div>
          </div>
          <div class="card mt-2" style="width: 40rem;">
         
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              
              <div class=" d-flex  " >
              <p class="card-text ">Some </p>
            </div>
            <div class="d-flex  justify-content-end">
              <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
            </div>
           
         
          
            </div>
          </div>
          <div class="card mt-2" style="width: 40rem; visibility: hidden;" >
         
              <div class="card-body">
                <h5 class="card-title">Card title</h5>
                
                <div class=" d-flex  " >
                <p class="card-text ">Some </p>
              </div>
              <div class="d-flex  justify-content-end">
                <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
              </div>
             
           
            
              </div>
            </div>
          <div class=" row bg-secondary fixed-bottom sticky-bottom" >
            
           
            
          
              <div class="  d-flex justify-content-center col-12  mb-5 mt-4 align-items-center "> 
                  <div class="btn btn-dark  " style="position:sticky; cursor: pointer">
                      <input type="date" name="selectdate">
                      <input type="time" name="selecttime">
                      <!-- <a href="vendor_list.htm" class="btn btn-secondory" style="font-weight: bold">Submit</a> -->
                      <button type="submit" id="elec" class="btn btn-secondary">Submit</button>
                  </div>
            </div>
            </div>
      </div>
      
      <script>
      $(document).ready(function() {
          $('#elec').click(function(){
              var favorite = [];
              $.each($("input[name='electrician']:checked"), function(){            
                  favorite.push($(this).val());
                  
              });
             // alert("My serices are: " + favorite.join(", "));
              window.location = "vendor_list.htm?arr="+favorite; 
          });
      });



      /*               
      $("#elec").prop("disabled", true);

		$.post("vendor_list.htm", {
  		
			favorite : favorite
			
		}, function(data) {

		var json = JSON.parse(data);
		//...

	}).done(function() {
	}).fail(function(xhr, textStatus, errorThrown) {
	}).complete(function() {
		$("#elec").prop("disabled", false);
				
	});

});

});

			
  }); */

      
      </script>
      <!-- <div class="col-3 " >
        <div class="btn btn-dark  " style="position:sticky; margin-left:100px; top:500px; cursor: pointer">
          NEXT
      </div>
    </div> -->
      </div>
  
  
    </div>
    <div class="col-3" onclick="off2()" >
       
        
      </div>
</div>
</div>
  <!--Trying 4 Plumber-->
<div class="" id="overlay3"  style="display: none;" >
    <div class="row">
      <div class="col-3"  onclick="off3()">
          <div class="btn btn-dark " style="position:sticky;margin-left:100px; top:500px; cursor: pointer">
              BACK
          </div>
      </div>
      <div class="col-6">
        <div class=" row bg-secondary fixed-top sticky-top" style="height: 70px">
          
         
          
        
        <div class=" col-12  d-flex justify-content-center align-items-center "> 
           <h1> Plumber</h1>
         
     
      </div>
      </div>
      <div class="row ">
          <!-- <div class="col-3 " >
              <div class="btn btn-dark " style="position:sticky;margin-left:100px; top:500px; cursor: pointer">
                BACK
            </div>
          </div> -->
      <div class="col-12">
        <div class="card mt-2" style="width: 40rem;">
       
          <div class="card-body">
            <h5 class="card-title">Pipe/Tap Fitting</h5>
            
            <div class=" d-flex  " >
            <p class="card-text ">Some </p>
          </div>
          <div class="d-flex  justify-content-end">
            <input type="checkbox" name="plumber" value="pipe" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
          </div>
       
        
          </div>
        </div>
        <div class="card mt-2" style="width: 40rem;">
       
          <div class="card-body">
            <h5 class="card-title">Water Leakages</h5>
            
            <div class=" d-flex  " >
            <p class="card-text ">Some </p>
          </div>
          <div class="d-flex  justify-content-end">
            <input type="checkbox" name="electrician" value="leakage" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
          </div>
       
        
          </div>
        </div>
        <div class="card mt-2" style="width: 40rem;">
       
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            
            <div class=" d-flex  " >
            <p class="card-text ">Some </p>
          </div>
          <div class="d-flex  justify-content-end">
            <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
          </div>
       
        
          </div>
        </div>
        <div class="card mt-2" style="width: 40rem;">
       
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            
            <div class=" d-flex  " >
            <p class="card-text ">Some </p>
          </div>
          <div class="d-flex  justify-content-end">
            <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
          </div>
       
        
          </div>
        </div>
        <div class="card mt-2" style="width: 40rem;">
       
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            
            <div class=" d-flex  " >
            <p class="card-text ">Some </p>
          </div>
          <div class="d-flex  justify-content-end">
            <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
          </div>
       
        
          </div>
        </div>
        <div class="card mt-2" style="width: 40rem;">
       
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            
            <div class=" d-flex  " >
            <p class="card-text ">Some </p>
          </div>
          <div class="d-flex  justify-content-end">
            <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
          </div>
        
          </div>
        </div>
        <div class="card mt-2" style="width: 40rem;">
       
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            
            <div class=" d-flex  " >
            <p class="card-text ">Some </p>
          </div>
          <div class="d-flex  justify-content-end">
            <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
          </div>
       
        
          </div>
        </div>
    </div>
    <!-- <div class="col-3 " >
      <div class="btn btn-dark  " style="position:sticky; margin-left:100px; top:500px; cursor: pointer">
        NEXT
    </div>
  </div> -->
    </div>


  </div>
  <div class="col-3"  onclick="off3()">
      <div class="btn btn-dark  " style="position:sticky; margin-left:100px; top:500px; cursor: pointer">
          <a href="www.google.com">NEXT</a>
      </div>
    </div>
</div>
</div>
<!--Trying 4 Cleaner-->
<div class="" id="overlay4"  style="display: none;" >
    <div class="row">
      <div class="col-3"  onclick="off4()">
          <div class="btn btn-dark " style="position:sticky;margin-left:100px; top:500px; cursor: pointer">
              BACK
          </div>
      </div>
      <div class="col-6">
        <div class=" row bg-secondary fixed-top sticky-top" style="height: 70px">
          
         
          
        
        <div class=" col-12  d-flex justify-content-center align-items-center "> 
           <h1> Plumber</h1>
         
     
      </div>
      </div>
      <div class="row ">
          <!-- <div class="col-3 " >
              <div class="btn btn-dark " style="position:sticky;margin-left:100px; top:500px; cursor: pointer">
                BACK
            </div>
          </div> -->
      <div class="col-12">
        <div class="card mt-2" style="width: 40rem;">
       
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            
            <div class=" d-flex  " >
            <p class="card-text ">Some </p>
          </div>
          <div class="d-flex  justify-content-end">
            <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
          </div>
       
        
          </div>
        </div>
        <div class="card mt-2" style="width: 40rem;">
       
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            
            <div class=" d-flex  " >
            <p class="card-text ">Some </p>
          </div>
          <div class="d-flex  justify-content-end">
            <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
          </div>
       
        
          </div>
        </div>
        <div class="card mt-2" style="width: 40rem;">
       
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            
            <div class=" d-flex  " >
            <p class="card-text ">Some </p>
          </div>
          <div class="d-flex  justify-content-end">
            <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
          </div>
       
        
          </div>
        </div>
        <div class="card mt-2" style="width: 40rem;">
       
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            
            <div class=" d-flex  " >
            <p class="card-text ">Some </p>
          </div>
          <div class="d-flex  justify-content-end">
            <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
          </div>
       
        
          </div>
        </div>
        <div class="card mt-2" style="width: 40rem;">
       
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            
            <div class=" d-flex  " >
            <p class="card-text ">Some </p>
          </div>
          <div class="d-flex  justify-content-end">
            <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
          </div>
       
        
          </div>
        </div>
        <div class="card mt-2" style="width: 40rem;">
       
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            
            <div class=" d-flex  " >
            <p class="card-text ">Some </p>
          </div>
          <div class="d-flex  justify-content-end">
            <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
          </div>
       
        
          </div>
        </div>
        <div class="card mt-2" style="width: 40rem;">
       
          <div class="card-body">
            <h5 class="card-title">Card title</h5>
            
            <div class=" d-flex  " >
            <p class="card-text ">Some </p>
          </div>
          <div class="d-flex  justify-content-end">
            <input type="checkbox" style="width: 30px;height: 30px" aria-label="Checkbox for following text input">
          </div>
       
        
          </div>
        </div>
    </div>
    <!-- <div class="col-3 " >
      <div class="btn btn-dark  " style="position:sticky; margin-left:100px; top:500px; cursor: pointer">
        NEXT
    </div>
  </div> -->
    </div>


  </div>
  <div class="col-3"  onclick="off4()">
      <div class="btn btn-dark  " style="position:sticky; margin-left:100px; top:500px; cursor: pointer">
          <a href="www.google.com">NEXT</a>
      </div>
    </div>
</div>
</div>

  <script>
  
	function emailVal(){

        var email = document.getElementById('staticEmail').value;
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        if (!isEmpty(email) && !filter.test(email)) {

        document.getElementById("mSpan3").innerHTML="Please provide a valid email address";
        document.getElementById("mSpan3").style.color="red";
        
        return false;
        }
        else{
            document.getElementById("mSpan3").innerHTML=" ";
            return true;
        }
    }
   
   
	    function passwordFun(){
        // var regName = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"/;
         var passid = document.getElementById("inputPassword").value;
		 //var passid1 = document.getElementById('inputPassword1').value;
         var passid_len = passid.length;
       //  window.alert(passid);
		 //var passid_len1 = passid.length1;
         if (passid_len == 0 || passid_len < 6 ) {
             document.getElementById("mSpan4").innerHTML = "Password should not be empty and length of 6-12";
             document.getElementById("mSpan4").style.color = "red";
             document.getElementById("inputPassword1").value="";
          //   $(this).focus();
             return false;
         } else {
             document.getElementById("mSpan4").innerHTML = "";
                 
             return true;
         }
     }


		function conValidate(){
			//var regName = /^[a-zA-Z]+$/;
	         var passid = document.getElementById("inputPassword").value;
			 var passid1 = document.getElementById("inputPassword1").value;
			// window.alert(passid1);
			 if (passid != passid1) {
				 document.getElementById("mSpan5").innerHTML = "You first Password is not similar with  this password ";
	             document.getElementById("mSpan5").style.color = "red";
				  return false; 
			 }else{
				 document.getElementById("mSpan5").innerHTML = "";
				   return true; 
				}

			}
	
   
    function isEmpty(value) {
        if (!value == "" || !value == "null" || !value == undefined) {
            return true;
        }
        else {
            return false;
        }
    }
	
	
	function validateAge() {
			
			var aNo = document.getElementById('inputAge').value;
    	    var aNo_len = aNo.length;
			var aMb=/^\S[1-9]{1,3}$/;
			if (!isEmpty(aNo) || !aMb.test(aNo)) {
				document.getElementById("mSpan2").innerHTML = "this is invalid age";
				document.getElementById("mSpan2").style.color = "red";
			return false;
			}else {
            document.getElementById("mSpan2").innerHTML = ""
            return true;
        }
	}
	
	
	function addressFun(){
         var regName = /^[a-zA-Z]+$/;
         var name = document.getElementById('add').value;
         if (!isEmpty(name) && !regName.test(name)) {
             document.getElementById("mSpan6").innerHTML = "Address should not be empty";
             document.getElementById("mSpan6").style.color = "red";

             return false;
         } else {
             document.getElementById("mSpan6").innerHTML = ""
                 
             return true;
         }
     }
  
  
	function firstName() {
        var regName = /^[a-zA-Z]+$/;
        var name = document.getElementById('inputName').value;
        if (!regName.test(name)) {
            document.getElementById("mSpan1").innerHTML = "this is invalid name ";
            document.getElementById("mSpan1").style.color = "red";

            return false;
        } else {
            document.getElementById("mSpan1").innerHTML = ""
            return true;
        }
    }
    
	  function val1() {

    	    var mNo = document.getElementById('inputPhone').value;
    	    var mNo_len = mNo.length;
    	    var mbnum=/^[0][1-9]\d{9}$|^[1-9]\d{9}$/
    	    if(!mbnum.test(mNo))
    	        {
    	            document.getElementById("mSpan").innerHTML="please Enter the Contact Number";
    	            document.getElementById("mSpan").style.color = "red";
    	            return false;
    	        }
    	        else{
    	            document.getElementById("mSpan").innerHTML=" ";
    	                return true;
    	        }
    	}
    	  
	
  function myFunction() {
   var x = document.getElementById("mySelect").value;
                if(x=="electrician"){
                     // console.log(" hii") ; 
                  on2();}
                 if(x=="plumber"){on3();}
               if(x=="cleaner"){on4();}
    }

      function on() {
        document.getElementById("overlay").style.display = "block";
        document.getElementById("overlay1").style.display = "none";
        document.getElementById("overlay2").style.display = "none";
        document.getElementById("overlay3").style.display = "none";
        document.getElementById("overlay4").style.display = "none";
      }
      
      function off() {
        document.getElementById("overlay").style.display = "none";
      }

      function on1() {
        document.getElementById("overlay1").style.display = "block";
        document.getElementById("overlay").style.display = "none";
        document.getElementById("overlay2").style.display = "none";
        document.getElementById("overlay3").style.display = "none";
        document.getElementById("overlay4").style.display = "none";
      }
      
      function off1() {
        document.getElementById("overlay1").style.display = "none";
      }

      function on2() {
        document.getElementById("overlay2").style.display = "block";
        document.getElementById("overlay1").style.display = "none";
        document.getElementById("overlay").style.display = "none";
        document.getElementById("overlay3").style.display = "none";
        document.getElementById("overlay4").style.display = "none";
      }
    
      
      function off2() {
        document.getElementById("overlay2").style.display = "none";
        document.getElementById("mySelect").value="nothing";
      }
      function on3() {
        document.getElementById("overlay3").style.display = "block";
        document.getElementById("overlay1").style.display = "none";
        document.getElementById("overlay").style.display = "none";
        document.getElementById("overlay2").style.display = "none";
        document.getElementById("overlay4").style.display = "none";

      }
      function off3() {
        document.getElementById("overlay3").style.display = "none";
        document.getElementById("mySelect").value="nothing";
      }
    
      function on4() {
        document.getElementById("overlay4").style.display = "block";
        document.getElementById("overlay1").style.display = "none";
        document.getElementById("overlay").style.display = "none";
        document.getElementById("overlay2").style.display = "none";
        document.getElementById("overlay3").style.display = "none";

      }
      function off4() {
        document.getElementById("overlay4").style.display = "none";
        document.getElementById("mySelect").value="nothing";
      }
      
      </script>
	
</body>
</html>