<%@page import="com.cdac.springwebapp.dto.User"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
 pageEncoding="ISO-8859-1"%>
  <%@ taglib uri="http://www.springframework.org/tags/form" prefix="spr" %> 
 
 <%
 	User vendor = (User)session.getAttribute("vendork");
 	User vendor1=(User)request.getAttribute("ul");
 
 %>   
 
<!DOCTYPE html>
<html>
<head>
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
     crossorigin="anonymous">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
     
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	    <div class="container-fluid ">
    <div class="row">
        <div class="col-3"></div>
        <div class="col-6 rounded bg-info mt-5">
            <div class="row">
                <div class="col-3">
                       <!-- <a href="prep_login_form.htm"><i class="fa fa-home ml-4 mt-2" style="font-size:40px"></i></a> -->
                 </div>

                <div class="col-6">
                    <h2 class=" d-flex justify-content-center"><%=vendor.getUserName()%></h2>
                </div>

                <div class="col-3">
                          <a href="logoutV.htm"><i class="fa fa-sign-out ml-5 mt-2" style="font-size:40px"></i></a>
                </div>
                
            </div>
            <p class="d-flex justify-content-center"><strong>About: </strong> <%=vendor.getAbout()%> </p>
            <p class=" d-flex justify-content-center"><strong>Address:  </strong> <%=vendor.getAddress()%> </p>
            <p class="d-flex justify-content-center"><strong>Phone No.: </strong><%=vendor.getPhoneNo()%> </p>
            <div class="row">
                <div class="col-1"></div>

                <div class="col-2 d-flex justify-content-center mb-2">
                        <a class="btn btn-primary btn-lg" onclick="update()" style="font-size: 12px; height: 45px; width: 100px"  role="button">Update</a>
                </div>
                <div class="col-2 d-flex justify-content-center mb-2">
                        <a class="btn btn-primary btn-lg" style="font-size: 12px; height: 45px; width: 100px" onclick="requestsList()"  role="button"> Requests</a>
                </div>
                <div class="col-2 d-flex justify-content-center mb-2">
                        <a class="btn btn-primary btn-lg" style="font-size: 12px; height: 45px; width: 100px" onclick="reviews()"  role="button"> Reviews</a>
                </div>
                <div class="col-2 d-flex justify-content-center mb-2">
                        <a class="btn btn-primary btn-lg"  onclick="orderLog()"   style="font-size: 12px ;height: 45px; width: 100px" href="#" role="button">Order Log</a>
                </div>
                <div class="col-2 d-flex justify-content-center mb-2">
                    <a class="btn btn-primary btn-lg"  onclick="tips()"   style="font-size: 12px ;height: 45px; width: 100px" href="#" role="button">Tips</a>
                </div>
                <div class="col-1"></div>
            </div>
          
            
        </div>
        <div class="col-3"  ></div>
            
        </div>
        <div class="row mt-2">
                <div class="col-3"></div>
                <div class="col-6 rounded bg-info mt-1">
                        <div id="div2" class="mt-3" style="display: none">
                                <table  class="table">
                                        <thead class="thead-dark">
                                                <tr>
                                                        <th>User Name</th>
                                                        <th>Description</th>
                                                        <th>Address</th>
                                                        <th>Accept/Reject</th>
                                                </tr>
                                        </thead>
                                        <tbody>
                                              
                                        </tbody>
                                </table>
                        </div>
                         <div id="div3" style="color:black;display: none">
                            <b>Guidlines For Venders:-</b>
                            <ol>
                                <li>
                                    Do not list services or offers relating to any service in a category 
                                    that is inapropriate to the Service that they are offering.
                                </li>
                                <li>
                                    Do not include barnd name and other inapropriate keywords in their profile,
                                    offetrs,feedback or any  other tittle or discription relating to a service.
                                </li>
                                <li>
                                    Do not use misleading tittle that describe the service.
                                </li>
                                <li>
                                    Venders futher clarify the their registration on the website shall be deeemed to 
                                    be your consent to be contacted for the purposes.
                                </li>
                                <li>
                                     Service professional's status earned via the feedback page will be affected.
                                </li>
                                <li>
                                     Block the Profile.
                                </li>
                            </ol>
                        </div>


                        <div id="div4" style="display: none">
                                <spr:form  action="update.htm" modelAttribute="ul">
                                        <div>
                                            <h1 style="margin-left: 230px ">Update Form</h1>
                                        </div>
                        
                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                                            <div class="col-sm-10">
                                            <spr:textarea  path="userName" class="form-control" id="inputPassword" placeholder="Enter your full Name"></spr:textarea>
                                            </div>
                                        </div>
                                        
                                            <spr:textarea  path="userProfession" class="form-control" id="inputPassword"  style="display:none;"   placeholder="Enter your full Name"></spr:textarea>
                                             <spr:textarea  path="userPass" class="form-control" id="inputPassword"  style="display:none;"   placeholder="Enter your full Name"></spr:textarea>
                        						 <spr:textarea  path="userRole" class="form-control" id="inputPassword"  style="display:none;"   placeholder="Enter your full Name"></spr:textarea>
                   <spr:textarea  path="userId" class="form-control" id="inputPassword"  style="display:none;"   placeholder="Enter your full Name"></spr:textarea>
                        				                   <spr:textarea  path="active" class="form-control" id="inputPassword"  style="display:none;"   placeholder="Enter your full Name"></spr:textarea>
          <spr:textarea  path="userConformPass" class="form-control" id="inputPassword"  style="display:none;"   placeholder="Enter your full Name"></spr:textarea>
                        						 
                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-2 col-form-label">Age</label>
                                            <div class="col-sm-10">
                                            <spr:textarea  path="age"  class="form-control" id="inputPassword" placeholder="Enter your age"></spr:textarea>
                                            </div>
                                        </div>
                        
                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-sm-2 col-form-label">Phone no</label>
                                            <div class="col-sm-10">
                                            <spr:textarea  path="phoneNo"   class="form-control" id="inputPassword" placeholder="Enter your 10 digit number"></spr:textarea>
                                            </div>
                                        </div>
                        
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
                                            <div class="col-sm-10">
                                            <spr:textarea path="emailId"    class="form-control" id="staticEmail" placeholder="Enter your email id"></spr:textarea>
                                            </div>
                                        </div>
       
                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-sm-2 col-form-label">About</label>
                                                <div class="col-sm-10">                        
                                                    <spr:textarea   path="about"  class="form-control" rows="2" placeholder="enter about"></spr:textarea>
                                                </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-sm-2 col-form-label">Address</label>
                                                <div class="col-sm-10">                        
                                                    <spr:textarea   path="address" class="form-control" rows="2" placeholder="enter address"></spr:textarea>
                                                </div>
                                        </div>
                        
                        <input type="submit" value="submit" class="btn btn-success" style="margin-left: 170px"/>
                                       
                        
                                    </spr:form>
                        </div>



                </div>
                <div class="col-3" ></div>
        </div>
       
    </div>


   
    <script>
          
            function requestsList() {
              document.getElementById("div2").style.display ="block";
              document.getElementById("div3").style.display ="none";  
              document.getElementById("div4").style.display ="none";
            }
            function reviews() {
              document.getElementById("div3");
               
            }
            function tips(){
                document.getElementById("div3").style.display ="block"; 
                document.getElementById("div2").style.display ="none";
                document.getElementById("div4").style.display ="none";
            }
            function update(){
                document.getElementById("div4").style.display ="block";
                document.getElementById("div2").style.display ="none";
                document.getElementById("div3").style.display ="none";
                
            }
        </script>
	
</body>
</html>