<%@page import="com.cdac.springwebapp.dto.User"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%
 	List<User> userList = (List<User>)request.getAttribute("ul1");
    String services = (String)request.getAttribute("subServList");
 %> 
<!DOCTYPE html>
<html>
<head>
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<table class="table">
		 <thead class="thead-dark">
                                                <tr>
                                                        <th>Vendor</th>
                                                        <th>About</th>
                                                        <th>Services</th>
                                                        <th>Select</th>
                                                        <th>Description</th>
                                                        <th><a class="btn btn-secondary" href="prep_login_form.htm">Back</a></th>
                                                </tr>
                                        </thead>
		   <tbody>
		<%
			for(User u : userList){
		%>
			<tr>
				
				<td><%=u.getUserName() %></td>
				<td><%=u.getAbout() %></td>
				<td><%=services%></td><!-- address -->
				<td><input name="service" value="" type="radio"></td>
				<td><%=u.getUserRole() %></td>
			</tr>
		<% }  %>
		</tbody>
	</table>
</body>
</html>